package com.project.coches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectCocheApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectCocheApplication.class, args);
	}

}
